package pl.edu.uwm.wmii.marcingalazka.laboratorium05;

public class Zadanie1 {
    public static void main(String[] args) {
        Adres adres1 = new Adres();
        adres1.pokaz();
        Adres adres2 = new Adres("Dworcowa", 55, "01-100", "Warszawa");
        adres2.pokaz();
    }
}
