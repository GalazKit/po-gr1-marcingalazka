package pl.edu.uwm.wmii.marcingalazka.laboratorium09;

public class PairUtil<T> {
    public <T> Pair swap (Pair<T> obj) {
        Pair<T> wynik = new Pair<T>();
        wynik.setFirst(obj.getSecond());
        wynik.setSecond(obj.getFirst());
        return wynik;
    }
}