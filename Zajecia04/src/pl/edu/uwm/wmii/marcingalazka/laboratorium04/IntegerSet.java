package pl.edu.uwm.wmii.marcingalazka.laboratorium04;

public class IntegerSet {

    public IntegerSet(){
        boolean[] tab = new boolean[99];
        for(int i = 0; i < tab.length; i++){
            tab[i] = false;
        }
    }

    boolean[] tab = new boolean[99];

    public static boolean[] union(boolean[] A, boolean[] B){
        int rozmiar;
        boolean[] C = new boolean[99];
        if(A.length > B.length){
            rozmiar = A.length;
        }
        else{
            rozmiar = B.length;
        }
        for(int i = 0; i < rozmiar; i++){
            if(A[i] == true || B[i] == true){
                C[i] = true;
            }
        }
        return C;
    }

    public static boolean[] intersection(boolean[] A, boolean[] B){
        boolean[] C = new boolean[99];

        int rozmiar;

        if(A.length > B.length){
            rozmiar = A.length;
        }
        else{
            rozmiar = B.length;
        }

        for(int i =0; i < rozmiar; i++){
            if(A[i] == true && B[i] == true){
                C[i] = true;
            }
        }
        return C;
    }


    public void insertElement(int n){tab[n-1] = true;}
    public void deleteElement(int n){tab[n-1] = false;}


    public String toString(){
        String str = "";
        for(int i = 0; i < tab.length; i++){
            if(tab[i] == true){
                str+= Integer.toString(i+1);
                str+= " ";
            }
        }
        return str;
    }
    public void Equals(IntegerSet u1){
        int l = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i] != u1.tab[i]){
                l++;
            }
        }
        if(l==0){
            System.out.println("Zbiory są identyczne.");
        }
        else{
            System.out.println("Zbiory nie są identyczne.");
        }
    }
}
