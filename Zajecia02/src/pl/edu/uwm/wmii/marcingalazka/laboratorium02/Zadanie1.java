package pl.edu.uwm.wmii.marcingalazka.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1 {
    public static int[] rng(int[] tab, int n){
        Random rng = new Random();
        for(int i = 0; i < n; i++){
            int a = rng.nextInt(1998) -999;
            tab[i] = a;
        }
        return tab;
    }
    public static void parnpar (int[] tab, int n){
        int p = 0;
        int np = 0;
        for(int i = 0; i < n; i++){
            if(tab[i] == 0){
                p++;
            }
            if(tab[i] != 0){
                np++;
            }
        }
        System.out.println("Liczb parzystych: "+p);
        System.out.println("Liczb nieparzystych: " +np);
    }
    public static void u0d(int[] tab, int n){
        int u = 0;
        int o = 0;
        int d = 0;
        for(int i = 0; i < n; i++){
            if(tab[i] <0) {
                u++;
            }
            if(tab[i] == 0){
                o++;
            }
            if(tab[i] > 0){
                d++;
            }
        }
        System.out.println("Liczb ujemnych: "+u);
        System.out.println("Liczb równych 0: "+o);
        System.out.println("Liczb dodatnich: "+d);
    }
    public static void maxilosc(int[] tab, int n){
        int l =0;
        int max = tab[0];
        for(int i = 0; i < n; i++){
            if(tab[i] > max){
                max = tab[i];
                l = 0;
            }
            if(max == tab[i]){
                l++;
            }
        }
        System.out.println("Największa liczba to "+max+", wystąpiła ona "+l+" razy");
    }
    public static void sumadodouj(int[] tab, int n){
        int sumad = 0;
        int sumau = 0;
        for(int i = 0; i<n; i++){
            if(tab[i] > 0){
                sumad += tab[i];
            }
            if(tab[i]<0){
                sumau+=tab[i];
            }
        }
        System.out.println("Suma liczb dodatnich wynosi "+sumad+", suma liczb ujemnych wynosi "+sumau);
    }
    public static void dodfrag(int[] tab, int n){
        int l = 0;
        int lp = 0;
        for(int i = 0; i<n; i++){
            if(tab[i]>0){
                lp++;
            }
            if(lp > l){
                l = lp;
            }
            if(tab[i]<0){
                lp = 0;
            }
        }
        System.out.println("Najdłuższy fragment dodatni ma "+l+" liczby.");
    }
    public static void zamiana(int[] tab, int n){
        for(int i = 0; i<n; i++){
            if(tab[i] < 0){
                tab[i] = -1;
            }
            else{
                tab[i] = 1;
            }
        }
    }
    public static void zamianalewpraw(int[] tab, int n){
        int lewy;
        int prawy;
        int x,z;
        int l = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        lewy = in.nextInt();
        System.out.println("Podaj drugą liczbe: ");
        prawy = in.nextInt();
        while(lewy > prawy){
            System.out.println("Druga liczba nie moze być mniejsza od pierwszej. Podaj jeszcze raz liczbę: ");
            prawy = in.nextInt();
        }
        lewy = lewy-1;
        prawy = prawy-1;
        for( int i = lewy; i <= prawy/2; i++){
            x = tab[i];
            z = tab[prawy-l];
            tab[prawy-l] = x;
            tab[i] = z;
            l++;

        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        int n;
        n = in.nextInt();
        int[] tab = new int[n];
        rng(tab, n);

        for(int i = 0; i < n; i++){
            System.out.println(tab[i]);
        }

        zamianalewpraw(tab, n);

        for(int i = 0; i < n; i++){
            System.out.println(tab[i]);
        }
    }
}
