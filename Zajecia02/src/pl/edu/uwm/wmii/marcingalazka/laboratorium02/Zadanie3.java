package pl.edu.uwm.wmii.marcingalazka.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie3 {

    public static int[][] macierz(int n, int m) {
        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                Random rng = new Random();
                array[i][j] = rng.nextInt(10);
            }
        }
        return array;
    }

    public static int[][] mnozeniemacierzy(int n, int m, int k) {
        int[][] array1 = macierz(n, m);
        int[][] array2 = macierz(m, k);
        int[][] array3 = macierz(n, k);
        int s;

        System.out.println("Macierz A:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print(array1[i][j] + ", ");
            }
            System.out.println();
        }

        System.out.println();
        System.out.println("Macierz B:");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < k; j++) {
                System.out.print(array2[i][j] + ", ");
            }
            System.out.println();
        }
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < k; ++j) {
                array3[i][j] = 0;
            }
        for (int i = 0; i < n; i++)
            for (int j = 0; j < k; j++)
                for (int z = 0; z < m; z++) {
                    array3[i][j] += array1[i][z] * array2[z][j];
                }
        return array3;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = 3, m = 2, k = 1;
        while (n > m && m > k) {
            System.out.println("Podaj pierwszy wymiar macierzy: ");

            n = in.nextInt();
            System.out.println("Podaj drugi wymiar macierzy: ");

            m = in.nextInt();
            System.out.println("Podaj trzeci wymiar macierzy: ");

            k = in.nextInt();
            if (n > m && m > k) {
                System.out.println("Wprowadzono złe wymiary! Pierwszy wymiar musi być mniejszy od drugiego, a drugi musi być mniejszy od trzeciego.");
                System.out.println();
            }
        }
        int[][] macierz = mnozeniemacierzy(n, m, k);
        System.out.println();
        System.out.println("Macierz AxB:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) {
                System.out.print(macierz[i][j] + ", ");
            }
            System.out.println();
        }
    }
}