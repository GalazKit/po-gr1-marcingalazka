package pl.edu.uwm.wmii.marcingalazka.laboratorium00;

import java.util.Arrays;

public class Lab00 {
    public static void main(String[] args) {
        System.out.println("Zad1");
        System.out.println("Styczen" + 31 + "\nLuty" + 28 + "\nMarzec"+31);

        System.out.println("\nZad2");
        System.out.println(1+2+3+4+5+6+7+8+9+10);

        System.out.println("\nzad3");
        System.out.println(1*2*3*4*5*6*7*8*9*10);

        System.out.println("\nzad4");
        int x =1000;
        int x1 = x + x*6/100;
        int x2 = x1 + x1*6/100;
        int x3 = x2 + x2*6/100;
        System.out.println("R1: " + x1 + " R2: " + x2 + " R3: " +x3);

        System.out.println("\nzad5");
        System.out.println("----------");
        System.out.println("|  Java  |");
        System.out.println("----------");

        System.out.println("\nzad6");
        System.out.println(" /////");
        System.out.println("+''''''+");
        System.out.println("(| * * |)");
        System.out.println("  | ^ | ");
        System.out.println(" | '-' |");
        System.out.println("+-----+");

        System.out.println("\nzad7");
        System.out.println("M           M         A         RRRRR     *************************************");
        System.out.println("M M       M M        A A        R    R    ***************************************");
        System.out.println("M  M     M  M       A   A       R     R   ****************************************");
        System.out.println("M   M   M   M      A     A      R    R    *****************************************");
        System.out.println("M    M M    M     A       A     RRRRR     *****************************************");
        System.out.println("M     M     M    AAAAAAAAAAA    R R       *****************************************");
        System.out.println("M           M   A           A   R  R      *****************************************");
        System.out.println("M           M  A             A  R   R     *****************************************");
        System.out.println("M           M A               A R    R    *****************************************");

        System.out.println("\nzad8");
        System.out.println("   +");
        System.out.println("  + +  ");
        System.out.println(" +   + ");
        System.out.println("+-----+");
        System.out.println("| .-. |");
        System.out.println("| | | |");
        System.out.println("+-+-+-+");

        System.out.println("\nzad9");
        System.out.println("  /\\_/\\        -----");
        System.out.println(" ( o o )     / Hello \\");
        System.out.println("/(  u  )\\    < Junior |");
        System.out.println("  | | |       \\Coder!/");
        System.out.println(" (__|__}       -----");

        System.out.println("\nzad10");
        System.out.println("Piraci z Karaibów");
        System.out.println("Pinokio");
        System.out.println("50 twarzy Greya");

        System.out.println("\nzad11");
        String[] strArray = new String[] {"Tej wiosny znowu ptaki wróciły za wcześnie. \n" +
                "Ciesz się, rozumie, instynkt też się myli. \n" +
                "Zagapi się, przeoczy - i spadają w śnieg, \n" +
                "i giną licho, giną nie na miarę \n" +
                "budowy swojej krtani i arcypazurtów, \n" +
                "rzetelnych chrząstek i sumiennych błon, \n" +
                "dorzecza serca, labiryntu jelit, \n" +
                "nawy żeber i kręgów w świetnej amfiladzie, \n" +
                "piórr gdnych pawilonu w muzeum wrzechrzemiosł \n" +
                "i dzioba mniszej cierpliwości. \n" +
                "\n" +
                "To nie jest lameny, to tylko zgorszenie, \n" +
                "że anioł z prawdziwego białka, \n" +
                "latawiec o gruczołach z pieśni nad pieśniami, \n" +
                "pojedynczy w powietrzu, nieprzeliczony w ręce, \n" +
                "tkanka po tkance związany we wspólność \n" +
                "miejsca i czasu jak sztuka klasyczna \n" +
                "w brawach skrzydeł - \n" +
                "spada i kładzie się obok kamienia, \n" +
                "który w stój archaiczny i prostacki sposób \n" +
                "patrzy na życie jak na odrzucane próbny. "};
        System.out.println(Arrays.toString(strArray));

        System.out.println("\nzad12");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");

    }
}
